@extends('layouts.app', ['activePage' => 'User', 'titlePage' => __('Edit Organizer')])

@section('content')
  <div class="content">
    <div class="container-fluid">
      <div class="row">
        <div class="col-md-12">
          <form method="post" action= "{{ route('updateOrg' , $organization->id)}}" autocomplete="off" class="form-horizontal">
            @csrf
            @method('put')

            <div class="card ">
              <div class="card-header card-header-success">
                <h4 class="card-title">{{ __('Edit Organization') }}</h4>
                <p class="card-category">{{ __('Organization information') }}</p>
              </div>
              <div class="card-body ">
                @if (session('status'))
                  <div class="row">
                    <div class="col-sm-12">
                      <div class="alert alert-success">
                        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                          <i class="material-icons">close</i>
                        </button>
                        <span>{{ session('status') }}</span>
                      </div>
                    </div>
                  </div>
                @endif
                <div class="row">
                  <label class="col-sm-2 col-form-label">{{ __('Name Of Organization') }}</label>
                  <div class="col-sm-7">
                    <div class="form-group{{ $errors->has('org_name') ? ' has-danger' : '' }}">
                      <input class="form-control{{ $errors->has('org_name') ? ' is-invalid' : '' }}" name="org_name" id="input-name" type="text" placeholder="{{ __('org_name') }}" value="{{ $organization->org_name }}" required="true" aria-required="true"/>
                      @if ($errors->has('org_name'))
                        <span id="name-error" class="error text-danger" for="input-name">{{ $errors->first('org_name') }}</span>
                      @endif
                    </div>
                  </div>
                </div>
                <div class="row">
                  <label class="col-sm-2 col-form-label">{{ __('Minimum topics') }}</label>
                  <div class="col-sm-7">
                    <div class="form-group{{ $errors->has('min_topics') ? ' has-danger' : '' }}">
                      <input class="form-control{{ $errors->has('min_topics') ? ' is-invalid' : '' }}" name="min_topics" id="input-min_topics" type="text" placeholder="{{ __('Minimum topics') }}" value="{{ $organization->min_topics }}" required />
                      @if ($errors->has('min_topics'))
                        <span id="min_topics-error" class="error text-danger" for="input-min_topics">{{ $errors->first('min_topics') }}</span>
                      @endif
                    </div>
                  </div>
                  </div>
                </div>
               
              
              <div class="card-footer ml-auto mr-auto">
                <button type="submit" class="btn btn-success">{{ __('Save') }}</button>
              </div>
            </div>
          </form>
        </div>
      </div>
      
    </div>
  </div>
 


@endsection

@section('js')
<script
  src="https://code.jquery.com/jquery-3.4.1.min.js"
  integrity="sha256-CSXorXvZcTkaix6Yvo6HppcZGetbYMGWSFlBw8HfCJo="
  crossorigin="anonymous"></script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.9/js/select2.min.js"></script>
<script>
$(document).ready(function() {
    $('.js-example-basic-multiple').select2();
});
</script>
@endsection

