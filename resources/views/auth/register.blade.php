@extends('layouts.app', ['class' => 'off-canvas-sidebar', 'activePage' => 'register', 'title' => __('Meeting Planner')])

@section('content')
<div class="container" style="height: auto;">
  <div class="row align-items-center">
    <div class="col-lg-4 col-md-6 col-sm-8 ml-auto mr-auto">
      <form class="form" method="POST" action="{{ route('register') }}">
        @csrf

        <div class="card card-login card-hidden mb-3">
          <div class="card-header card-header-warning text-center">
            <h4 class="card-title"><strong>{{ __('Register') }}</strong></h4>
          </div>
          <div class="card-body ">
            
            <div class="bmd-form-group{{ $errors->has('name') ? ' has-danger' : '' }}">
              <div class="input-group">
                <div class="input-group-prepend">
                  <span class="input-group-text">
                      <i class="material-icons">account_circle</i>
                  </span>
                </div>
                <input type="text" name="name" class="form-control" placeholder="{{ __('Name..') }}" value="{{ old('name') }}" required>
              </div>
              @if ($errors->has('name'))
                <div id="name-error" class="error text-danger pl-3" for="name" style="display: block;">
                  <strong>{{ $errors->first('name') }}</strong>
                </div>
              @endif
            </div>
            <div class="bmd-form-group{{ $errors->has('email') ? ' has-danger' : '' }} mt-3">
              <div class="input-group">
                <div class="input-group-prepend">
                  <span class="input-group-text">
                    <i class="material-icons">email</i>
                  </span>
                </div>
                <input type="email" name="email" class="form-control" placeholder="{{ __('Email..') }}" value="{{ old('email') }}" required>
              </div>
              @if ($errors->has('email'))
                <div id="email-error" class="error text-danger pl-3" for="email" style="display: block;">
                  <strong>{{ $errors->first('email') }}</strong>
                </div>
              @endif
            </div>
            
            <div class="bmd-form-group{{ $errors->has('password') ? ' has-danger' : '' }} mt-3">
              <div class="input-group">
                <div class="input-group-prepend">
                  <span class="input-group-text">
                    <i class="material-icons">lock_outline</i>
                  </span>
                </div>
                <input type="password" name="password" id="password" class="form-control" placeholder="{{ __('Password..') }}" required>
              </div>
              @if ($errors->has('password'))
                <div id="password-error" class="error text-danger pl-3" for="password" style="display: block;">
                  <strong>{{ $errors->first('password') }}</strong>
                </div>
              @endif
            </div>
            <div class="bmd-form-group{{ $errors->has('password_confirmation') ? ' has-danger' : '' }} mt-3">
              <div class="input-group">
                <div class="input-group-prepend">
                  <span class="input-group-text">
                    <i class="material-icons">lock_outline</i>
                  </span>
                </div>
                <input type="password" name="password_confirmation" id="password_confirmation" class="form-control" placeholder="{{ __('Confirm Password..') }}" required>
              </div>
              @if ($errors->has('password_confirmation'))
                <div id="password_confirmation-error" class="error text-danger pl-3" for="password_confirmation" style="display: block;">
                  <strong>{{ $errors->first('password_confirmation') }}</strong>
                </div>
              @endif
            </div>
            <div class="bmd-form-group{{ $errors->has('org_id') ? ' has-danger' : '' }} mt-3">
              <div class="input-group">
                <div class="input-group-prepend">
                  <span class="input-group-text">
                    <i class="material-icons">group_work</i>
                  </span>
                </div>
                <input type="number" name="org_id" id="org_id" class="form-control" placeholder="{{ __('Organization ID..') }}" value="{{ old('org_id') }}" >
              </div>
              @if ($errors->has('org_id'))
                <div id="org_id-error" class="error text-danger pl-3" for="org_id" style="display: block;">
                  <strong>{{ $errors->first('org_id') }}</strong>
                </div>
              @endif
            </div><br> 
            <div style="text-align:center"> <h5>If You Dont have the Org ID</h5> </div>
          <div class="bmd-form-group"><div class="input-group-text"></div></div>
           <div class="bmd-form-group">
              <div class="input-group">
                <div class="input-group-prepend">
                  <span class="input-group-text">
                   <input type="checkbox" id="neworg" onclick="
                    var orgNameInput = document.getElementById('org_name');
                    var minTopicsInput = document.getElementById('min_topics');
                    var orgIDInput = document.getElementById('org_id');

                    if(this.checked) {
                        orgNameInput.disabled = false;
                        minTopicsInput.disabled = false;
                        orgIDInput.disabled = true;
                        orgNameInput.focus();
                    } else {
                        orgNameInput.disabled = true;
                        minTopicsInput.disabled = true;
                        orgIDInput.disabled = false;

                    };"/>Create a new Organization
                   </span>
                </div>
              </div>
            </div>



            
            <div class="bmd-form-group{{ $errors->has('org_name') ? ' has-danger' : '' }} mt-3">
              <div class="input-group">
                <input type="text" name="org_name" disabled="disabled" id="org_name" class="form-control on" placeholder="{{ __('Org Name...') }}" value="{{ old('org_name') }}">
              </div>
              @if ($errors->has('org_name'))
                <div id="org_name-error" class="error text-danger pl-3" for="org_name" style="display: block;">
                  <strong>{{ $errors->first('org_name') }}</strong>
                </div>
              @endif
            </div>
            <div class="bmd-form-group{{ $errors->has('min_topics') ? ' has-danger' : '' }} mt-3">
              <div class="input-group">

                <input type="number" disabled="disabled" name="min_topics" id="min_topics" class="form-control mt" placeholder="{{ __('Minimum topics...') }}" value="{{ old('min_topics') }}" >
              </div>
              @if ($errors->has('min_topics'))
                <div id="min_topics-error" class="error text-danger pl-3" for="min_topics" style="display: block;">
                  <strong>{{ $errors->first('min_topics') }}</strong>
                </div>
              @endif
            </div>
            <div class="form-check mr-auto ml-3 mt-3">
              <label class="form-check-label">
                <input class="form-check-input" type="checkbox" id="policy" name="policy" {{ old('policy', 1) ? 'checked' : '' }} >
                <span class="form-check-sign">
                  <span class="check"></span>
                </span>
                {{ __('I agree with  ') }} <a href="#">{{ __('Privacy Policy') }}</a>
              </label>
            </div>
          </div>
          <div class="card-footer justify-content-center">
            <button type="submit" class="btn btn-success btn-warning btn-lg">{{ __('Create account') }}</button>
          </div>
        </div>
      </form>

    </div>
  </div>
</div>
@endsection
