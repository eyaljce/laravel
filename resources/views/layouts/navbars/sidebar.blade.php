<div class="sidebar" data-color="azure" data-background-color="white">
  <div class="logo">
    <a class="simple-text logo-normal">
      {{ __('MeetApp Team') }}
    </a>
  </div>
  <div class="sidebar-wrapper">
    <ul class="nav">
      <li class="nav-item{{ $activePage == 'dashboard' ? ' active' : '' }}">
        <a class="nav-link" href="{{ route('dashboard') }}">
          <i class="material-icons">dashboard</i>
            <p>{{ __('Dashboard') }}</p>
        </a>
      </li>
      <li class="nav-item {{ ($activePage == 'profile' || $activePage == 'user-management') ? ' active' : '' }}">
        <div class="collapse show" id="laravelExample">
          <ul class="nav">
            <li class="nav-item{{ $activePage == 'profile' ? ' active' : '' }}">
              <a class="nav-link" href="{{ route('profile.edit') }}">
              <i class="material-icons">account_circle</i>
                <span class="sidebar-normal">{{ __('User profile') }} </span>
              </a>
            </li>
            <li class="nav-item{{ $activePage == 'user-management' ? ' active' : '' }}">
              <a class="nav-link" href="{{ route('user.index') }}">
              <i class="material-icons">supervised_user_circle</i>
                <span class="sidebar-normal"> {{ __('Edit User Information') }} </span>
              </a>
            </li>
            <li class="nav-item{{ $activePage == 'User' ? ' active' : '' }}">
              <a class="nav-link" href="{{ route('editOrg') }}">
              <i class="material-icons">edit</i>
                <span class="sidebar-normal"> {{ __('Edit Organization Info') }} </span>
              </a>
            </li>
          </ul>
        </div>
      </li>
      <li class="nav-item{{ $activePage == 'table' ? ' active' : '' }}">
        <a class="nav-link" href="{{ route('table') }}">
          <i class="material-icons">content_paste</i>
            <p>{{ __('Meeting List') }}</p>
        </a>
      </li>
      @cannot('participant')
      <li class="nav-item{{ $activePage == 'notifications' ? ' active' : '' }}">
        <a class="nav-link" href="{{ route('notifications') }}">
          <i class="material-icons">format_list_bulleted</i>
            <p>{{ __('Manage Tasks') }}</p>
        </a>
      </li>
      @endcannot
     
    </ul>
  </div>
</div>