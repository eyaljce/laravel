@extends('layouts.app', ['activePage' => 'table', 'titlePage' => __('Meeting List')])
<!-- <link href="{!! asset('css/app.min.css') !!}" media="all" rel="stylesheet" type="text/css" /> -->

@section('content')
<div class="content">

@if($isBetween)
  <div class="container-fluid">
    <div class="row">
      <div class="col-md-12">
        <div class="card">
          <div class="card-header card-header-success">
            <h4 class="card-title "><span>{{$meeting->title}}</span> 
  </span></h4>
            <span class="card-category">{{$meeting->length}} hours    </span>
            <html>

</html>
          </div>

          <div class="card-body">
            <div class="table-responsive">
            <div class="tab-content">
                  <div class="tab-pane active col-md-6" id="profile">
                    <table class="table">
                    <th>Please check discussed topics</th>
                      <tbody>
                        @foreach($topics as $topic)
                        <tr> @if($topic->meeting_id==$meeting->id)
                          <th>
                          <div class="form-check">
                              <label class="form-check-label">
                              <input id="{{$topic->id}}" class="toggle-class form-check-input" type="checkbox" data-toggle="toggle" {{ $topic->status ? 'checked' : '' }}>
                                <span class="form-check-sign">
                                  <span class="check"></span>
                                </span>
                              </label>
                            </div>
                          </th>

                            
                             <td scope="row">{{$topic->title}}</td>
                             
                             @endif
                             </tr>
                            @endforeach
                            </tbody>
                          
                        </tr>
                        <tr>

                      
                        </tr>
                      </tbody>
                    </table>
            </div><br>

                    <div class="card">
          <div class="card-header card-header-success">
            <h4 class="card-title ">Tasks  </h4>
          </div>

          <div class="card-body">

            <div class="table-responsive">
            <div class="tab-content">
                  <div class="tab-pane active col-md-6" id="profile">
                    <table class="table">
                    <th scope="col">Task</th>
                    <th scope="col">User</th>
                    <th scope="col">Due Date</th>

                  

                      <tbody>
                      @foreach($tasks as $task)
                        
                          @if($task->meeting_id==$meeting->id)
                          <tr>
                          <td>{{$task->title}}</td>
                          <td>{{$task->user->name}}</td>
                          <td>{{$task->due_date}}</td>

                          </tr>
                          @endif
                      @endforeach
                        
                      </tbody>
                    </table>
                    </div>
                  </div>
                    <h5 style="font-weight: bold;">Add new Task</h5>
                    <form method = 'POST' action ="{{route('createTask', ['id'=>$meeting->id])}}">
                    @csrf
                    <div class="input col-md-3">
                      <input class="form-control" type="text" id='title' name='title' placeholder="Title">
                      <input class="form-control" name="due_date" id="due_date" type="date" placeholder="{{ __('Due Date') }}">
                      <select class="form-control" name="user_name" placeholder="Addign To">
                            <option class="form-control" value="" selected disabled>Assign To</option>
                                      @foreach($users as $user)
                                    @if($user->org_id==Auth::user()->org_id)                            
                                          <option class="form-control" value="{{ $user->id }}">{{ $user->name }}</option>
                                    @endif
                                    @endforeach
                      </select>       
                      <button type="submit" class="btn btn-success" value = "Save">{{ __('Add') }}</button>
                      
                    </div>

                    </form>      
                    <br><br><br>    
                    Please refresh this page before hitting the "Finish Meeting" button. <br>
                    <div style="text-align:center">
      <button type="submit" class="btn btn-success" value = "Save" onclick="document.getElementById('hidden').style.display = 'block'">{{ __('Finish Meeting') }}</button>
      </div>
      <div style="display:none" class=hidden id=hidden>
<br><br>
      @if($topics->where('status', '=', '0')->where('meeting_id', '=', $meeting->id)->count()>0)
      <h3>Notice you have some undiscussed topics</h3>

      <form method = 'POST' action="{{action('MeetingController@moveTopics', ['id'=>$meeting->id])}}">
      @csrf
      <div class="card-body">
            <div class="table-responsive">
            <div class="tab-content">
                  <div class="tab-pane active col-md-6" id="profile">
                    <table class="table">
                    <th>Topic</th>
                    <th>Switch to Meeting</th>
                      <tbody>
                        @foreach($topics as $topic)
                        <tr> @if($topic->meeting_id==$meeting->id && $topic->status == 0)
                                                   
                             <td scope="row">{{$topic->title}}</td>
                             <td>
                             <select class="js-example-basic-multiple col-sm-11" name="topics[ {{ $topic->id }} ]">
                             <option value="{{ $topic->meeting_id }}">Choose another meeting</option>

                            @foreach($meetings as $meeting)
                                @if($meeting->user->org_id==Auth::user()->org_id)                            
                                 <option value="{{ $meeting->id }}">{{ $meeting->title}}</option>
                                @endif
                            @endforeach
                            </select> </td>
                             @endif
                             </tr>
                            @endforeach
                            </tbody>
                          
                        </tr>
                        <tr>

                      
                        </tr>
                      </tbody>
                    </table>
            </div><br>
            <button type="submit" class="btn btn-success" value = "Save">{{ __('Submit') }}</button>
      </form>
      @else
      <h3>No undiscussed topics remained, meeting has ended</h3>
      
      </div>
  
  
      </div>
  </div>
</div>
@endif
@else
<br><br><br>
<h2  class="text-center text-dark font-weight-bold">Scheduled Meeting Can't Start yet</h2>
<h4 class="text-center text-danger"></h4>
<div style="text-align:center">

<td class="td-actions " ><a href="{{ route('table') }}" class="btn btn-sm btn-success" >{{ __('Back to the List') }}</a></td>
</div> 
@endif



<script>
// Set the date we're counting down to
var countDownDate = ({{$meeting->date->addHours($meeting->length)}};

// Update the count down every 1 second
var x = setInterval(function() {

  // Get today's date and time
  var now = new Date().getTime();

  // Find the distance between now and the count down date
  var distance = countDownDate - now;

  // Time calculations for days, hours, minutes and seconds
  var hours = Math.floor((distance % (1000 * 60 * 60 * 24)) / (1000 * 60 * 60));
  var minutes = Math.floor((distance % (1000 * 60 * 60)) / (1000 * 60));

  // Display the result in the element with id="demo"
  document.getElementById("demo").innerHTML =  hours + "h "
  + minutes + "m ";

  // If the count down is finished, write some text
  if (distance < 0) {
    clearInterval(x);
    document.getElementById("demo").innerHTML = "MEETING ENDED";
  }
}, 1000);
</script>






<script>document.getElementById('timer').innerHTML =
 {{$meeting->length}}*60 + ":" + 00;
startTimer();

function startTimer() {
  var presentTime = document.getElementById('timer').innerHTML;
  var timeArray = presentTime.split(/[:]+/);
  var m = timeArray[0];
  var s = checkSecond((timeArray[1] - 1));
  if(s==59){m=m-1}
  if(m<0){alert('Meeting Has Ended')}
  
  document.getElementById('timer').innerHTML =
    m + ":" + s;
  setTimeout(startTimer, 1000);
}

function checkSecond(sec) {
  if (sec < 10 && sec >= 0) {sec = "0" + sec}; // add zero in front of numbers < 10
  if (sec < 0) {sec = "59"};
  return sec;
}</script>
@endsection
@push('js')

<script>
      $(document).ready(function(){
           $(":checkbox").change(function(event){
                   console.log(event);
                   $.ajax({
                   url:"{{url('topics')}}" + '/' + event.target.id,
                   dataType: 'json',
                   type: 'put' ,
                   contentType: 'application/json',
                   data: JSON.stringify({'status':event.target.checked, _token:'{{csrf_token()}}'}),
                   processData:false,
                   success: function( data){
                        console.log(JSON.stringify( data ));
                   },
                   error: function(errorThrown ){
                       console.log( errorThrown );
                   }
               });               
           });
       });
   </script> 
@endpush
