@extends('layouts.app', ['activePage' => 'table', 'titlePage' => __('User Profile')])
@section('content')

  <div class="content">
    <div class="container-fluid">
      <div class="row">
        <div class="col-md-12">
          <form method="post" action = "{{action('MeetingController@update' , $meeting->id)}}" autocomplete="off" class="form-horizontal">
            @csrf
            @method('PATCH')

            <div class="card ">
              <div class="card-header card-header-success">
                <h4 class="card-title">{{ __('Edit Meeting') }}</h4>
                <p class="card-category">{{ __('Basic Information') }}</p>
              </div>
              <div class="card-body ">
                @if (session('status'))
                  <div class="row">
                    <div class="col-sm-12">
                      <div class="alert alert-success">
                        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                          <i class="material-icons">close</i>
                        </button>
                        <span>{{ session('status') }}</span>
                      </div>
                    </div>
                  </div>
                @endif
                <div class="row">
                  <label class="col-sm-2 col-form-label">{{ __('Title') }}</label>
                  <div class="col-sm-7">
                    <div class="form-group{{ $errors->has('title') ? ' has-danger' : '' }}">
                      <input class="form-control{{ $errors->has('title') ? ' is-invalid' : '' }}" name="title" id="input-title" type="text" placeholder="{{ __('Title') }}" value = "{{$meeting->title}}" required="true" aria-required="true"/>
                      @if ($errors->has('title'))
                        <span id="title-error" class="error text-danger" for="input-title">{{ $errors->first('title') }}</span>
                      @endif
                    </div>
                  </div>
                </div>
                <div class="row">
                  <label class="col-sm-2 col-form-label">{{ __('Location') }}</label>
                  <div class="col-sm-7">
                    <div class="form-group{{ $errors->has('location') ? ' has-danger' : '' }}">
                      <input class="form-control{{ $errors->has('location') ? ' is-invalid' : '' }}" name="location" id="input-location" type="text" placeholder="{{ __('Location') }}" value = "{{$meeting->location}}" required />
                      @if ($errors->has('location'))
                        <span id="location-error" class="error text-danger" for="input-location">{{ $errors->first('location') }}</span>
                      @endif
                    </div>
                  </div>
                </div>
                
                <div class="row">
                  <label class="col-sm-2 col-form-label">{{ __(' Date & Time:') }}</label>
                  <div class="col-sm-7">
                    <div class="form-group">
                      <input class="form-control" name="date" id="input-date" type="datetime-local" placeholder="{{ __('Date') }}" value = "{{$meeting->date->format('Y-m-d\TH:i')}}"  />
                     
                    </div>
                  </div>
                </div>
                
                <div class="row">
                  <label class="col-sm-2 col-form-label">{{ __('Length') }}</label>
                  <div class="col-sm-7">
                    <div class="form-group{{ $errors->has('length') ? ' has-danger' : '' }}">
                      <input class="form-control{{ $errors->has('length') ? ' is-invalid' : '' }}" name="length" id="input-location" type="double" placeholder="{{ __('Length') }}" value = "{{$meeting->length}}" required />
                      @if ($errors->has('length'))
                        <span id="length-error" class="error text-danger" for="input-length">{{ $errors->first('length') }}</span>
                      @endif
                    </div>
                  </div>
                </div>
                <br><br>
                <div class="card-header card-header-success">
                <p class="card-category">{{ __('Topics') }}</p>
              </div>
<br>
            @foreach($topics as $topic)
            @if($topic->meeting_id==$meeting->id)
              <div class="row">
                  <label class="col-sm-2 col-form-label">{{ __('Topic') }}</label>
                  <div class="col-sm-7">
                    <div class="form-group{{ $errors->has('location') ? ' has-danger' : '' }}">
                      <input class="form-control{{ $errors->has('topics') ? ' is-invalid' : '' }}" name="topics[]" id="input-topics" type="text" placeholder="{{ __('topics') }}" value = "{{$topic->title}}"  required />
                      @if ($errors->has('topics'))
                        <span id="topics-error" class="error text-danger" for="input-topics">{{ $errors->first('topics') }}</span>
                      @endif
                    </div>
                  </div>
                </div>
                @endif
            @endforeach
            <br>
            <h4><a href ="{{route('createTopic', $meeting->id)}}">Add topic </a></h4>
            <br><br>
                <div class="card-header card-header-success">
                <p class="card-category">{{ __('Invites') }}</p>
              </div>
            <br>
            <div class="row">
                  <label class="col-sm-2 col-form-label">{{ __('Invites') }}</label>
                  <div class="col-sm-7">
                    <div class="form-group{{ $errors->has('location') ? ' has-danger' : '' }}">
                    <select class="js-example-basic-multiple col-sm-7" name="invites[]" multiple="multiple">
            @foreach($invites as $invite)
                @if($invite->meeting_id==$meeting->id)                            
                            <option value="{{ $invite->user_id }}" selected>{{ $invite->user->name}}</option>
                @endif
            @endforeach
            @foreach($users as $user)
                @if($user->org_id==Auth::user()->org_id)                            
                            <option value="{{ $user->id }}">{{ $user->name}}</option>
                @endif
            @endforeach
            </select> 
            </div>
            </div>
            </div>
            </div>
              <div class="card-footer ml-auto mr-auto">
              <button type="submit" class="btn btn-success" value = "Save">{{ __('Save') }}</button>
            </div>
            </div>
          </form>
        </div>
      </div>






</form>
@csrf
<!-- add following if asked for verification -->
@if ($errors->any())
            <div class="alert alert-danger">
                <ul>
                    @foreach ($errors->all() as $error)
                        <li>{{ $error }}</li>
                    @endforeach
                </ul>
            </div>
        @endif



@endsection

@section('js')
<script
  src="https://code.jquery.com/jquery-3.4.1.min.js"
  integrity="sha256-CSXorXvZcTkaix6Yvo6HppcZGetbYMGWSFlBw8HfCJo="
  crossorigin="anonymous"></script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.9/js/select2.min.js"></script>
<script>
$(document).ready(function() {
    $('.js-example-basic-multiple').select2();
});
</script>
@endsection