@extends('layouts.app', ['activePage' => 'notifications', 'titlePage' => __('Notifications')])

@section('content')

<div class="content">
  <div class="container-fluid">
    <div class="row">
      <div class="col-md-12">
        <div class="card">
          <div class="card-header card-header-success">
            <h4 class="card-title ">Manage Tasks</h4>
            <p class="card-category"> </p>
            <div class="row">

                </div>
          </div>
          <div class="card-body">
            <div class="table-responsive">
              <table class="table">
        <tr>
        <th></th>
            <th scope="col">Meeting ID</th>
            <th scope="col">Task Title</th>
            <th scope="col">Due Date</th>
            <th scope="col">Assigned To</th>
            <th scope="col">Status</th>
            <th scope="col" class="td-actions text-right">Delete</th>

        </tr>
        <tbody>
        @foreach($tasks as $task)
          @if($task->creator_id==Auth::user()->id)
            <tr>
            <td>
            
                <td>{{$task->meeting_id}}</td>
                <th scope="row"> {{$task->title}}</th>
                <td>{{$task->due_date}}</td>
                <td>{{$task->user->name}}</td>
                @if($task->status == 1)
                <td>Done</td>
                @else 
                <td>Not done</td>
                @endif
                <td class="td-actions text-right">
                  <form action="" method="post">
                 @csrf
                 @method('delete')
                  <a rel="tooltip" class="btn btn-danger btn-link" href="{{route('deleteTask', $task->id)}}" data-original-title="" title="" onclick="confirm('{{ __("Are you sure you want to delete this?") }}') ? this.parentElement.submit() : ''">
                    <i class="material-icons">close</i>
                    <div class="ripple-container"></div>
                   </button>
                  </form>
                </td>
            </tr>
        @endif
        @endforeach
        </tbody>
    </table>
           
      </div>
    </div>
  </div>
</div>

@endsection

