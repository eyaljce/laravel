@extends('layouts.app', ['activePage' => 'details', 'titlePage' => __('Details Meeting')])

@section('content')
<div class="content">
  <div class="container-fluid">
    <div class="row">
      <div class="col-md-12">
        <div class="card">
          <div class="card-header card-header-success">
            <h4 class="card-title ">My Meetings</h4>
            <p class="card-category"> Previous Meetings</p>
            <div class="row">

                </div>
          </div>
          <div class="card-body">
          <h5 style="font-weight: bold;"> The details of the meeting </h5>
          
          <ul>
          <div class="col-10 text-right">
            </div>
            <li> Meeting Title : {{$meeting->title}} </li>
            <li> Location : {{$meeting->location}} </li>
            <li> Date & Time : {{$meeting->date}} </li>
            <li> Length : {{$meeting->length}} hours </li> 
            <li> Topics :</li>
            <ul>
            @foreach($topics as $topic)
            @if($topic->meeting_id==$meeting->id)
            <li> {{$topic->title}} </li>
            @endif
            @endforeach
            </ul>
            <li> Task :</li>
            <ul>
            @foreach($tasks as $task)
            @if($task->meeting_id==$meeting->id)
            <li> {{$task->title}} - 
            @if($task->status == 0) 
            Not Done
            @else
            Done 
            @endif
            </li>
            @endif
            @endforeach
            </ul>
            <li> Invites :</li>
            <ul>
            @foreach($invites as $invite)
            @if($invite->meeting_id==$meeting->id)
            <li> {{$invite->user->name}} </li>
            @endif
            @endforeach
            </ul>

            </ul>
            </div>
        </div> 
        
      </div>
    </div>
  </div>
</div>
@endsection
