@extends('layouts.app', ['activePage' => 'table', 'titlePage' => __('Meeting List')])

@section('content')
<div class="content">
  <div class="container-fluid">
    <div class="row">
      <div class="col-md-12">
        <div class="card">
          <div class="card-header card-header-success">
            <h4 class="card-title ">My Meetings</h4>
            <p class="card-category"> Upcoming Meetings</p>
            <div class="row">

                </div>
          </div>
          <div class="card-body">
            <div class="table-responsive">
              <table class="table">
              @cannot('participant')  
              <div class="col-12 text-right">
                    <a href="{{ route('create') }}" class="btn btn-sm btn-success">{{ __('Create a new meeting') }}</a>
              </div>
              @endcannot
        <tr>
            <th scope="col">#</th>
            <th scope="col">Meeting Title</th>
            <th scope="col">Location</th>
            <th scope="col">Date & Time</th>
            <th scope="col">Length</th>
            <th scope="col" class="th-actions text-right"></th>
            <th scope="col" class="th-actions text-right"> </th>
            <th scope="col" class="th-actions text-right"> </th>


        </tr>
        <tbody>
        @foreach($meetings as $meeting)
            <tr>
                <th scope="row">{{$meeting->id}}</th>
                <td>{{$meeting->title}}</td>
                <td>{{$meeting->location}}</td>
                <td>{{$meeting->date}}</td>
                <td>{{$meeting->length}} hours</td>
                <td class="td-actions"><a href="https://www.google.com/calendar/render?action=TEMPLATE&text={{urlencode($meeting->title)}}&dates={{$meeting->date->format('Ymd\THis\Z')}}/{{$meeting->date->addHours($meeting->length)->format('Ymd\THis\Z')}}&details=For+details,+link+here:+http://www.example.com&location={{urlencode($meeting->location)}}&sf=true&output=xml" target="_blank" class="btn btn-sm btn-success"><i class="material-icons">sync</i> {{ __('Google+') }}</a></td>
                @if($meeting->creator_id == Auth::user()->id)
                <td class="td-actions text-right"><a href="{{route('meeting.show',$meeting->id)}}" class="btn btn-sm btn-success"><i class="material-icons">play_arrow</i> {{ __('Start') }}</a></td>
                <td class="td-actions text-right">
                              <form action="" method="post">
                                  @csrf
                                  @method('delete')
                              
                                  <a rel="tooltip" class="btn btn-success btn-link" href="{{route('meetings.edit', $meeting->id)}}" >
                                    <i class="material-icons">edit</i>
                                    <div class="ripple-container"></div>
                                  </a>
                                  <a rel="tooltip" class="btn btn-danger btn-link" href="{{route('delete', $meeting->id)}}" data-original-title="" title="" onclick="confirm('{{ __("Are you sure you want to delete this?") }}') ? this.parentElement.submit() : ''">
                                    <i class="material-icons">close</i>
                                    <div class="ripple-container"></div>
                                  </a>
                                  <meta name="csrf-token" content="{{ csrf_token() }}">
    
                              </form>
                            
                          </td>
              @else
              
              @can('manager') 
              <td class="td-actions text-right"><a href="{{route('meeting.show',$meeting->id)}}" class="btn btn-sm btn-success"><i class="material-icons">play_arrow</i> {{ __('Start') }}</a></td>

                <td class="td-actions text-right">
                              <form action="" method="post">
                                  @csrf
                                  @method('delete')
                              
                                  <a rel="tooltip" class="btn btn-success btn-link" href="{{route('meetings.edit', $meeting->id)}}" >
                                    <i class="material-icons">edit</i>
                                    <div class="ripple-container"></div>
                                  </a>
                                  <a rel="tooltip" class="btn btn-danger btn-link" href="{{route('delete', $meeting->id)}}" data-original-title="" title="" onclick="confirm('{{ __("Are you sure you want to delete this?") }}') ? this.parentElement.submit() : ''">
                                    <i class="material-icons">close</i>
                                    <div class="ripple-container"></div>
                                  </a>
                                  <meta name="csrf-token" content="{{ csrf_token() }}">
    
                              </form>
                            
                          </td>
              @endcan
              @endif
              
            </tr>
        @endforeach
        </tbody>
    </table>
           
      </div>
    </div>
  </div>
</div>

@endsection
