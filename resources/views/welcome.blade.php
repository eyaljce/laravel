@extends('layouts.app', ['class' => 'off-canvas-sidebar', 'activePage' => 'home', 'titlePage' => __('Meeting Planner'), 'title' => __('Meeting Planner')])

@section('content')
<div class="container" style="height: auto;">
  <div class="row justify-content-center">
      <div class="col-lg-9 col-md-11">
          <h1 class="text-white text-center">{{ __('Welcome to Meeting Planner') }}</h1>
      </div>
  </div>
</div>
@endsection
