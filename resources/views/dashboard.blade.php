  @extends('layouts.app', ['activePage' => 'dashboard', 'titlePage' => __('Dashboard')])

  @section('content')
    <div class="content">
      <div class="container-fluid">

     

        <div class="row">
          <div class="col-lg-5 col-md-6 col-sm-6">
            <div class="card card-stats">
              <div class="card-header card-header-warning card-header-icon">
                <div class="card-icon">
                  <i class="material-icons">account_circle</i>
                </div>
                <p class="card-category" style="text-align:left">Name</p>
                <h3 class="card-title" style="text-align:left">{{ Auth::user()->name}}  
                </h3>
              </div>
              <div class="card-footer">
              <i>Role: {{ Auth::user()->role}}  </i>
              </div>
            </div>
          </div>
          
          <div class="col-lg-4 col-md-6 col-sm-6">
            <div class="card card-stats">
              <div class="card-header card-header-warning card-header-icon">
                <div class="card-icon">
                  <i class="fa fa-user"></i>
                </div>
                <p class="card-category" style="text-align:left">Organization Users Count</p>
                <h3 class="card-title" style="text-align:left">{{$users}}</h3>
              </div>
              <div class="card-footer">
              <i>Organization ID: {{ Auth::user()->org_id}} </i>*to invite members
              </div>
            </div>
          </div>
        </div>
        </div>
        
        <div class="row">
        <div class="col-lg-3 col-md-6 col-sm-6">
            <div class="card card-stats">
              <div class="card-header card-header-success card-header-icon">
                <div class="card-icon">
                  <i class="material-icons">calendar_today</i>
                </div>
                <p class="card-category" style="text-align:left">Upcoming Meetings</p>
                <h3 class="card-title" style="text-align:left">{{ $myMeetings }}</h3>
              </div>
              <div class="card-footer">
               
              </div>
            </div>
          </div>
          <div class="col-lg-3 col-md-6 col-sm-6">
            <div class="card card-stats">
              <div class="card-header card-header-success card-header-icon">
                <div class="card-icon">
                  <i class="material-icons">format_list_numbered</i>
                </div>
                <p class="card-category" style="text-align:left">My Tasks</p>
                <h3 class="card-title" style="text-align:left">{{ $openTasks }}</h3>
              </div>
              <div class="card-footer">
              </div>
            </div>
          </div>
        <div class="col-lg-3 col-md-6 col-sm-6">
          <div class="card card-stats">
            <div class="card-header card-header-danger card-header-icon">
              <div class="card-icon">
                 <i class="material-icons">close</i>
               </div>
                <p class="card-category" style="text-align:left">Overdue Tasks</p>
                <h3 class="card-title" style="text-align:left">{{ $OverDueTask }}</h3>
               </div>
               <div class="card-footer">
               </div>
             </div>
           </div>
          </div>
        <div class="row">
          <div class="col-lg-8 col-md-12">
            <div class="card">
              <div class="card-header card-header-tabs card-header-success">
                <div class="nav-tabs-navigation">
                  <div class="nav-tabs-wrapper">
                    <h4><span class="nav-tabs-title">My Tasks</span></h4>
                  </div>
                </div>
              </div>
              <div class="card-body">
                <div class="tab-content">
                  <div class="tab-pane active" id="profile">
                    <table class="table">
                      <tbody>
                        <tr>
                          
                          <thead class=" text-dark">
                          <th></th>
                          <th>Title</th>
                          <th>Due Date</th>
                          </thead>
                          @foreach($tasks as $task)
                          @if($task->user_id==Auth::user()->id)

                          <td>
                          
                            <div class="form-check">
                              <label class="form-check-label">
                              <input id="{{$task->id}}" class="toggle-class form-check-input" type="checkbox" data-toggle="toggle" {{ $task->status ? 'checked' : '' }}>
                                <span class="form-check-sign">
                                  <span class="check"></span>
                                </span>
                              </label>
                            </div>
                          </td>
                          @if ($task->status == 1)
                          <td><s>{{$task->title}}</s></td>
                          @else
                          <td>{{$task->title}}</td>
                          @endif
                          <td>{{$task->due_date}}</td>
                          </tr>
                          @endif
                          @endforeach
                       
                        <tr>

                      
                        </tr>
                  
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  @endsection

  @push('js')
    <script>
      $(document).ready(function() {
        // Javascript method's body can be found in assets/js/demos.js
        md.initDashboardPageCharts();
      });


      $(document).ready(function(){
           $(":checkbox").change(function(event){
                   console.log(event);
                   $.ajax({
                   url:"{{url('tasks')}}" + '/' + event.target.id,
                   dataType: 'json',
                   type: 'put' ,
                   contentType: 'application/json',
                   data: JSON.stringify({'status':event.target.checked, _token:'{{csrf_token()}}'}),
                   processData:false,
                   success: function( data){
                        console.log(JSON.stringify( data ));
                   },
                   error: function(errorThrown ){
                       console.log( errorThrown );
                   }
               });               
           });
       });
</script>
  @endpush