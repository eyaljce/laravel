<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Meeting;
use App\User;
use App\Topic;
use App\Invite;
use App\Task;
use App\Organization;
use JavaScript;
use Carbon\Carbon;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Gate;


class MeetingController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        if (Auth::check()) {
            $id = Auth::id();
            $user = User::find($id);
            $invites = Invite::find($user);
            $myMeetings = $user->futureMeetings;

            return view('pages.meeting_list', ['meetings' => $myMeetings]);
        }

        return redirect()->intended('/dashboard');
    }
    public function passedMeetings()
    {
        if (Auth::check()) {
            $id = Auth::id();
            $user = User::find($id);
            $invites = Invite::find($user);
            $myMeetings = $user->passedMeetings;
            return view('pages.passed_meetings', ['meetings' => $myMeetings]);
        }

        return redirect()->intended('/dashboard');
    }
    public function dashboard()
    {

        if (Auth::check()) {
            $topBarTasks = Auth::user()->tasks->count();
            $chartData = [];
            $chartKeys = [];
            $myTasks = Task::where('created_at', '>=', Carbon::now()->firstOfYear())
                ->where('user_id', '=', Auth::user()->id)
                ->get()
                ->groupBy(function ($val) {
                    return Carbon::parse($val->created_at)->format('M');
                });
            $keys = $myTasks->keys();
            $chartKeys = $keys->all();

            foreach ($myTasks as $month) {
                array_push($chartData, $month->count());
            }
            JavaScript::put(['chartData' => $chartData, 'chartKeys' => $chartKeys, 'topBarTasks' => $topBarTasks]);
            $topBarTopics = Auth::user()->tasks->count();
            $chartData1 = [];
            $chartKeys1 = [];
            $discussedTopics = Topic::where('status', '=', 1)
                // ->where($topic->meeting->org_id, '=', Auth::user()->org_id)
                ->get()
                ->groupBy('meeting_id')
            ;
            $keys = $discussedTopics->keys();
            $chartKeys = $keys->all();

            foreach ($discussedTopics as $discussedTopic) {
                array_push($chartData1, $discussedTopic->count());
            }
            JavaScript::put(['chartData1' => $chartData1, 'chartKeys1' => $chartKeys1, 'topBarTopics' => $topBarTopics]);
            $id = Auth::id();
            $user = User::find($id);
            $tasks = Task::all();
            $dt = Carbon::now();
            $myTasks = $user->tasks;
            $users = User::where('org_id', '=', $user->org_id)->count();
            $openTasks = Task::where('status', '=', '0')->where('user_id', '=', $user->id)->count();
            $OverDueTask = Task::where('status', '=', '0')->where('user_id', '=', $user->id)->where('due_date', '<', $dt)->count();
            $myMeetings = $user->futureMeetings->count();
            $data = ['openTasks' => $openTasks, 'myMeetings' => $myMeetings, 'tasks' => $tasks, 'OverDueTask' => $OverDueTask, 'dt' => $dt, 'users' => $users];
            return view('dashboard', $data);
        }

        return redirect()->intended('/dashboard');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function createTopic($id)
    {
        if (Auth::check()) {
            if (Gate::denies('manager')) {
                if (Gate::denies('planner')) {
                    abort(403, "Are you a hacker or what?");
                }
            }
            $meeting = Meeting::find($id);

            return view('meetings.add_topic', ['meeting' => $meeting]);
        }
        return redirect()->intended('/dashboard');
    }
    public function create()
    {
        if (Auth::check()) {
            if (Gate::denies('manager')) {
                if (Gate::denies('planner')) {
                    abort(403, "Are you a hacker or what?");
                }
            }

            return view('meetings.create', [
                'organization' => Auth::user()->organization,
                'users' => User::all()

            ]);
        }
        return redirect()->intended('/dashboard');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    // public function store(Request $request)
    // {
    //     $product = $request->session()->get('product');
    //     $product->save();
    //     return redirect('/products');
    // }
    public function addTopic(Request $request, $id)
    {
        if (Auth::check()) {
            if (Gate::denies('manager')) {
                if (Gate::denies('planner')) {
                    abort(403, "Are you a hacker or what?");
                }
            }
            $this->validate(
                $request,
                [
                    'title' => 'required',
                ]
            );
            $topic = new Topic;
            $topic->title = $request->title;
            $topic->meeting_id = $id;
            $topic->status = 0;
            $topic->save();
            return redirect('table-list');
        }
        return redirect()->intended('/dashboard');
    }

    public function store(Request $request)
    {

        if (Auth::check()) {
            if (Gate::denies('manager')) {
                if (Gate::denies('planner')) {
                    abort(403, "Are you a hacker or what?");
                }
            }
            //add following if asked for verification
            $user = Auth::user();
            $organization = $user->org_id;
            $this->validate(
                $request,
                [
                    'title' => 'required',
                    'location' => 'required',
                    'date' => 'required|date|after:yesterday',
                    // 'topics'=>'required|array| min:'.$organization->min_topics,
                    'topics.*' => 'string',
                ]
            );
            $date = Carbon::create(str_replace('T', ' ', $request->date) . ':00');
            //

            $meeting = new Meeting;

            $meeting->org_id = $user->org_id;
            $meeting->title = $request->title;
            $meeting->location = $request->location;
            $meeting->date = $date;
            $meeting->length = $request->length;

            $meeting->creator_id = $user->id;
            //$meeting->status = 0;
            $meeting->save();
            //topic storing:
            foreach ($request->topics as $topicTitle) {
                Topic::create([
                    'meeting_id' => $meeting->id,
                    'title' => $topicTitle,
                    'status' => 0,

                ]);
            }
            foreach ($request->invites as $invite) {
                Invite::create([
                    'meeting_id' => $meeting->id,
                    'user_id' => $invite,
                ]);
            }
            return redirect('table-list');
        }
        return redirect()->intended('/dashboard');
    }

    public function addtask($id)
    {
        if (Auth::check()) {
            if (Gate::denies('manager')) {
                if (Gate::denies('planner')) {
                    abort(403, "Are you a hacker or what?");
                }
            }

            return view('startmeeting');
        }
        return redirect()->intended('/dashboard');
    }
    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        if (Auth::check()) {
            $meeting = Meeting::find($id);
            $meetings = Meeting::all();
            $topics = Topic::all();
            $tasks = Task::where('meeting_id', '=', $meeting->id)->with('user', 'creator')->get();
            $users = User::all();
            $isBetween = now()->between($meeting->date, $meeting->date->addHours($meeting->length));
            $undiscussedTopics = Topic::where('status', '=', '0')->where('meeting_id', '=', $meeting->id)->count();

            return view('meetings.startmeeting', ['meeting' => $meeting, 'meetings' => $meetings, 'undiscussedTopics' => $undiscussedTopics, 'isBetween' => $isBetween, 'topics' => $topics, 'tasks' => $tasks, 'users' => $users]);
        }
        return redirect()->intended('/dashboard');
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        if (Auth::check()) {
            if (Gate::denies('manager')) {
                if (Gate::denies('planner')) {
                    abort(403, "Are you a hacker or what?");
                }
            }
            $meeting = Meeting::find($id);
            $topics = Topic::all();
            $invites = Invite::all();
            $users = User::all();
            return view('meetings.edit', ['meeting' => $meeting, 'topics' => $topics, 'users' => $users, 'invites' => $invites]);
        }
        return redirect()->intended('/dashboard');
    }

    public function editOrg()
    {
        if (Auth::check()) {
            if (Gate::denies('manager')) {
                abort(403, "Are you a hacker or what?");
            }
            $id = Auth::user()->org_id;
            $organization = Organization::find($id);

            return view('users.editOrg', ['organization' => $organization]);
        }
        return redirect()->intended('/dashboard');
    }


    public function updateOrg(Request $request, $id)
    {
        if (Auth::check()) {
            if (Gate::denies('manager')) {
                abort(403, "Are you a hacker or what?");
            }
            $this->validate(
                $request,
                [
                    'org_name' => 'required',
                    'min_topics' => 'required',
                ]
            );

            $organization = Organization::find($id);
            $data = $request->except(['_token']);
            $organization->update($data);

            return redirect('dashboard');
        }
        return redirect()->intended('/dashboard');
    }
    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        if (Auth::check()) {
            if (Gate::denies('manager')) {
                if (Gate::denies('planner')) {
                    abort(403, "Are you a hacker or what?");
                }
            }
            $this->validate(
                $request,
                [
                    'title' => 'required',
                    'location' => 'required',
                    'date' => 'required|date|after:yesterday',
                ]
            );
            $meeting = Meeting::find($id);
            $date = Carbon::create(str_replace('T', ' ', $request->date) . ':00');
            $data = $request->except(['_token']);
            if ($data['date'] !== null) {
                $data['date'] = $date;
            }
            $meeting->update($data);
            Topic::where('meeting_id', '=', $id)->delete();
            foreach ($request->post('topics', []) as $topic) {
                Topic::create([
                    'meeting_id' => $meeting->id,
                    'title' => $topic,
                    'status' => 0,
                ]);
            }

            Invite::where('meeting_id', '=', $id)->delete();
            foreach ($request->post('invites', []) as $invite) {
                Invite::create([
                    'meeting_id' => $meeting->id,
                    'user_id' => $invite,
                ]);
            }
            return \App::make('redirect')->to('/table-list')->with('flash_success', 'Thank you,!');
        }
        return redirect()->intended('/dashboard');
    }

    public function details($id)
    {
        if (Auth::check()) {
            $meeting = Meeting::find($id);
            $topics = Topic::all();
            $invites = Invite::all();
            $tasks = Task::all();

            return view('pages.details', ['meeting' => $meeting, 'topics' => $topics, 'invites' => $invites, 'tasks' => $tasks]);
        }
        return redirect()->intended('/dashboard');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {

        if (Auth::check()) {
            if (Gate::denies('manager')) {
                if (Gate::denies('planner')) {
                    abort(403, "Are you a hacker or what?");
                }
            }
            Meeting::find($id)->delete($id);
            $topics = Topic::all();
            $invites = Invite::all();
            foreach ($topics as $topic) {
                if ($topic->meeting_id == $id) {
                    $topic->delete();
                }
            }
            foreach ($invites as $invite) {
                if ($invite->meeting_id == $id) {
                    $invite->delete();
                }
            }


            return redirect('table-list');
        }
        return redirect()->intended('/dashboard');
    }

    public function deleteTask($id)
    {

        if (Auth::check()) {
            if (Gate::denies('manager')) {
                if (Gate::denies('planner')) {
                    abort(403, "Are you a hacker or what?");
                }
            }
            Task::find($id)->delete($id);

            return redirect('notifications');
        }
        return redirect()->intended('/dashboard');
    }
    public function sendInvite(request $request)
    {
        if (Auth::check()) {
            if (Gate::denies('manager')) {
                abort(403, "Are you a hacker or what?");
            }
            $to_name = $request->name;
            $to_email = $request->email;
            $data = array('name' => 'Meeting Planner', 'body' => 'A test mail');
            Mail::send('emails.mail', $data, function ($message) use ($to_name, $to_email) {
                $message->to($to_email, $to_name)
                    ->subject('Meeting Planner Invitation');
                $message->from('noammaoz23@gmail.com', 'Test Mail');
            });
            return back()->withStatus(__('Invite successfully sent'));
        }
        return redirect()->intended('/dashboard');
    }

    public function tasks()
    {
        if (Auth::check()) {

            $id = Auth::id();
            $user = User::Find($id);
            $tasks = $user->tasks;
            return view('pages.notifications', ['tasks' => $tasks]);
        }

        //     $id = Auth::id();
        //     $user = User::Find($id);
        //     $meetings = $user->meetings;
        //  return view('pages.table_list', ['meetings'=>$meetings]);       
        return redirect()->intended('/dashboard');
    }
    public function manageTasks() 
    {

        if (Auth::check()) {
            if (Gate::denies('manager')) {
                if (Gate::denies('planner')) {
                    abort(403, "Are you a hacker or what?");
                }
            }
            $id = Auth::id();
            $user = User::find($id);
            $tasks = Task::all();
            $dt = Carbon::now();

            return view('pages.notifications', ['tasks' => $tasks, 'dt' => $dt]);
        }
        return redirect()->intended('/dashboard');
    }


    public function changeStatus($id, Request $request)
    {
        if (Auth::check()) {
            $task = Task::find($id);
            $task->status = $request->status;
            $task->updated_at = now();
            $task->save();

            return response()->json(['success' => 'Horray! You have completed this task.']);
        }
        return redirect()->intended('/dashboard');
    }
    public function changeTopicStatus($id, Request $request)
    {

        if (Auth::check()) {
            $topic = Topic::find($id);
            $topic->status = $request->status;
            $topic->save();

            return response()->json(['success' => 'Horray! You have completed this task.']);
        }
        return redirect()->intended('/dashboard');
    }
    public function moveTopics($id, Request $request)
    {
        if (Auth::check()) {
            $topics = Topic::where('meeting_id', $id);
            foreach ($request->topics as $topicId => $meetingId) {
                Topic::find($topicId)->update(['meeting_id' => $meetingId]);
            }
        }
        return redirect('/dashboard');
    }
    public function createTask($id, Request $request)
    {
        if (Auth::check()) {
            if (Gate::denies('manager')) {
                if (Gate::denies('planner')) {
                    abort(403, "Are you a hacker or what?");
                }
            }
            $this->validate($request, [
                'title' => 'required',
                'due_date' => 'required|date|after:yesterday',
            ]);
            $task = new Task;
            $task->status = 0;
            $task->creator_id = Auth::id();
            $task->meeting_id = $id;
            $user = User::find($request->user_name);
            $task->user_id = $user->id;
            $task->due_date = $request->due_date;
            $task->title = $request->title;
            $task->save();

            return redirect()->action('MeetingController@show', ['id' => $id]);
        }
        return redirect()->intended('/dashboard');
    }
}
