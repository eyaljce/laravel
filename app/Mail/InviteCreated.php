<?php

namespace App\Mail;
use App\Invite;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

class InviteCreated extends Mailable
{
    use Queueable, SerializesModels;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct(Invite $invite)
    {
        $this->invite = $invite;
    }


    public function build()
    {
        return $this->from('noamco@post.jce.ac.il')->view('emails.invite', ['invite' => $this->invite]);   
    }
}
