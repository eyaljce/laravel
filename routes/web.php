<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('auth.login');
});
Route::get('/dashboard', 'HomeController@index')->name('home')->middleware('auth');
Auth::routes();
Route::group(['middleware' => 'auth'], function () {
    Route::resource('user', 'UserController', ['except' => ['show']]);
    Route::get('profile', ['as' => 'profile.edit', 'uses' => 'ProfileController@edit']);
    Route::put('profile', ['as' => 'profile.update', 'uses' => 'ProfileController@update']);
    Route::put('profile/password', ['as' => 'profile.password', 'uses' => 'ProfileController@password']);
});
Route::group(['middleware' => 'auth'], function () {
    Route::get('table-list', 'MeetingController@index')->name('table');
    Route::get('/dashboard', 'MeetingController@dashboard')->name('dashboard');
    Route::post('sendInvite', 'MeetingController@sendInvite')->name('sendInvite');
    Route::put('sendInvite', 'MeetingController@sendInvite')->name('sendInvite');

    Route::get('notifications', 'MeetingController@manageTasks')->name('notifications');
    Route::get('/meeting/{id}', 'MeetingController@show')->name('meeting.show');
    Route::get('editorg', 'MeetingController@editOrg')->name('editOrg');
    Route::put('editorg/{id}', 'MeetingController@updateOrg')->name('updateOrg');
});

Route::get('meetings/delete/{id}', 'MeetingController@destroy')->name('delete');
Route::get('meetings/addTopic/{id}', 'MeetingController@addTopic')->name('addTopic');
Route::get('meetings/Topic/{id}', 'MeetingController@createTopic')->name('createTopic');
Route::get('meetings/details/{id}', 'MeetingController@details')->name('details');
Route::get('meetings/deleteTask/{id}', 'MeetingController@deleteTask')->name('deleteTask');
Route::post('/meeting/{id}/moveTopics', 'MeetingController@moveTopics')->name('moveTopics');

Route::get('changeStatus', 'MeetingController@changeStatus');
Route::put('topics/{id}', 'MeetingController@changeTopicStatus');
Route::post('/meeting/{id}/tasks', 'MeetingController@createTask')->name('createTask');
Route::put('tasks/{id}', 'MeetingController@changeStatus');
Route::resource('meetings', 'MeetingController');
Route::post('meetings/store', 'MeetingController@store')->name('store');
Route::get('meetings/create', 'MeetingController@create')->name('create');


Route::get('editorg', 'MeetingController@editOrg')->name('editOrg');
Route::put('editorg/{id}', 'MeetingController@updateOrg')->name('updateOrg');
